package ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class RPNCalculator {
	
	private Map<String, Operation> operations;
	
	public RPNCalculator(){
		this.operations = new HashMap<String, Operation>();
		init();
	}
	
	
	private void init(){
		this.addOperation(new Add(), "+");
		this.addOperation(new Substract(), "-");
		this.addOperation(new Multiply(), "*");
		this.addOperation(new Division(), "/");
		this.addOperation(new Mod(), "MOD");
		this.addOperation(new MultiAdd(), "++");
		this.addOperation(new MultiSubstract(), "--");
		this.addOperation(new MultiDivision(), "//");
		this.addOperation(new MultiMultiply(), "**");
	}
	
	
	public void addOperation(Operation operation, String key){
		this.operations.put(key, operation);
	}
	
	
	private Float parserExpression(String expression)
	{
		try{
			ArrayList<Float> numbers = new ArrayList<Float>();
			String[] splits = expression.split(" ");
			Operation operation = null;
			for(String split : splits){
				try{
					operation = this.operations.get(split);
					numbers = operation.eval(numbers);
				} catch(Exception e){
					try{
						numbers.add(Float.parseFloat(split)); 
					} catch(Exception f){
						throw new IllegalArgumentException();
					}
				}
			}
			return numbers.get(0);
		}catch(Exception h){
			throw new IllegalArgumentException();
		}
	}
	
	
    public float eval(String expression) {
    	return this.parserExpression(expression);
    }
    
    

}